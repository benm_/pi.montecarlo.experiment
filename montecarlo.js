montecarlopi = function(canvasId, pointCount)
{
    var canvas = document.getElementById(canvasId);
    if(canvas==null)return;    
    var context = canvas.getContext("2d");
        
    var r360 = 2*Math.PI;    
    var inside = 0;    
    
    //get image data
    var imageDat = context.createImageData(canvas.width, canvas.height);
    
    for(var i=0;i<pointCount;i++)
    {
        //random point on the unit square
        var x = Math.random();
        var y = Math.random();
           
        //corrosponding point on the canvas
        var px = Math.round(x*canvas.width);
        var py = Math.round(y*canvas.height);
        //start position in the imageData array
        var pstart = py*canvas.width*4+px*4;       

        //if inside quarter circle
        if((x*x + y*y)<=1)
        {
            inside+=1;
            //set to RED-ish
            imageDat.data[pstart] = 200;
            imageDat.data[pstart+1] = 0;
            imageDat.data[pstart+2] = 0;
            imageDat.data[pstart+3] = 255;
        }
        else
        {
            //set to GREY
            imageDat.data[pstart] = 200;
            imageDat.data[pstart+1] = 200;
            imageDat.data[pstart+2] = 200;
            imageDat.data[pstart+3] = 255;
        }    
    }
    
    //CALCULATE PI
    var pi = (inside/pointCount)*4;
    
    //draw
    context.putImageData(imageDat,0,0);    
    context.fillStyle = '#000';
    context.font ="20px Arial";
    context.fillText("PI \u2245 " + pi, 10,20);
}